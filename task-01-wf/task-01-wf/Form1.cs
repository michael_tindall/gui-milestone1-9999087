﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_01_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cmb1.DataSource = ConvertList();
        }
        public static string[] ConvertList()
        {
            var conversions = new string[2] { "Kilometers", "Miles" };
            return conversions;
        }
        private void cmb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl2.Text = $"You have selected {cmb1.SelectedItem.ToString()}";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            lbl3.Text = "This button no worky";
            var input = 0.0;
            var result = 0.0;
            input = int.Parse(txt1.Text);
            if (cmb1.SelectedItem.ToString() == "Miles")
            {
                result = input * 1.609;
                lbl3.Text = $"{input} Miles is {result} Kilometers";
            }
            else
            {
                result = input * 0.621;
                lbl3.Text = $"{input} Kilometers is {result} Miles";
            }
        }
    }
}
