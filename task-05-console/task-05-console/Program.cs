﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var compnum = 0;
            var usernum = 0;
            var score = 0;
            var counter = 0;
            do
            {
                Console.WriteLine("Enter a number between 1 and 5.");
                usernum = int.Parse(Console.ReadLine());
                while ((usernum < 0) || (usernum > 5))
                {
                    Console.WriteLine("Enter a number between 1 and 5.");
                    usernum = int.Parse(Console.ReadLine());
                }
                Random rnd = new Random();
                compnum = rnd.Next(1, 6);
                Console.WriteLine($"You chose {usernum} and the computer chose {compnum}");
                if (usernum == compnum)
                {
                    score = score + 1;
                    Console.WriteLine($"You scored a point for a total of {score}");
                }
                counter++;
            } while (counter < 5);
            Console.WriteLine($"You ended with a total score of {score}");
        }
    }
}
