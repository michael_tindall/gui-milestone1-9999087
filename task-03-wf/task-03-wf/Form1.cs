﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cmb1.DataSource = DrinksList();
        }
        public static string[] DrinksList()
        {
            var drinks = new string[6] { "Bottled Water", "Lemonade", "Coca Cola", "Pepsi", "Orange Juice", "Tomato Juice" };
            return drinks;
        }

        private void cmb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl2.Text = $"You have selected {cmb1.SelectedItem.ToString()}";
        }
    }
}
