﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            var useritems = new double[5] { 0.0, 0.0, 0.0, 0.0, 0.0 };
            var result = 0.0;
            var resultgst = 0.0;
            useritems[0] = int.Parse(txt1.Text);
            useritems[1] = int.Parse(txt2.Text);
            useritems[2] = int.Parse(txt3.Text);
            useritems[3] = int.Parse(txt4.Text);
            useritems[4] = int.Parse(txt5.Text);
            result = useritems[0] + useritems[1] + useritems[2] + useritems[3] + useritems[4];
            resultgst = result * 1.15;
            lbl2.Text = $"Total Cost: {result}";
            lbl3.Text = $"Total Cost + GST: {resultgst}";
        }
    }
}
