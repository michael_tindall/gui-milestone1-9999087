﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var drinks = new string[6] { "Bottled Water", "Lemonade", "Coca Cola", "Pepsi", "Orange Juice", "Tomato Juice" };
            var userinput = 0;
            Console.WriteLine("Choose one drink from below");
            Console.WriteLine("1. Bottled Water");
            Console.WriteLine("2. Lemonade");
            Console.WriteLine("3. Coca Cola");
            Console.WriteLine("4. Pepsi");
            Console.WriteLine("5. Orange Juice");
            Console.WriteLine("6. Tomato Juice");
            userinput = int.Parse(Console.ReadLine());
            if (userinput == 1)
            {
                Console.WriteLine("You have chosen Bottled Water.");
            }
            else if (userinput == 2)
            {
                Console.WriteLine("You have chosen Lemonade.");
            }
            else if (userinput == 3)
            {
                Console.WriteLine("You have chosen Coca Cola.");
            }
            else if (userinput == 4)
            {
                Console.WriteLine("You have chosen Pepsi.");
            }
            else if (userinput == 5)
            {
                Console.WriteLine("You have chosen Orange Juice.");
            }
            else if (userinput == 6)
            {
                Console.WriteLine("You have chosen Tomato Juice.");
            }
            else
            {
                Console.WriteLine("You have not chosen a drink.");
            }
        }
    }
}
