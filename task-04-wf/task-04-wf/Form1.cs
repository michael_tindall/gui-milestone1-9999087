﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_04_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            var fruit = new Dictionary<string, string>();
            fruit.Add("apple", "");
            fruit.Add("banana", "");
            fruit.Add("orange", "");
            fruit.Add("lemon", "");
            fruit.Add("tomato", "");
            fruit.Add("pumpkin", "");
            var answer = "";
            if (fruit.TryGetValue(txt1.Text, out answer))
            {
                lbl2.Text = txt1.Text + " is in stock";
            }
            else
            {
                lbl2.Text = "No matches found";
            }
        }
    }
}
