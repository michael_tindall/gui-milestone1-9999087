﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var userchoice = "";
            var userinput = 0.0;
            var result = 0.0;
            Console.WriteLine("Please choose to convert [K]ilometers to Miles or [M]iles to Kilometers");
            userchoice = Console.ReadLine();
            while ((userchoice != "k") && (userchoice != "m"))
            {
                Console.WriteLine("Please enter a K to convert Kilometers to Miles or an M to convert Miles to Kilometers");
                userchoice = Console.ReadLine();
            }
            Console.WriteLine("Enter the value you would like to be converted");
            userinput = int.Parse(Console.ReadLine());
            if (userchoice == "m")
            {
                result = userinput * 1.609;
                Console.WriteLine($"{userinput} Miles is {result} Kilometers");
            }
            else
            {
                result = userinput * 0.621;
                Console.WriteLine($"{userinput} Kilometers is {result} Miles");
            }
        }
    }
}
