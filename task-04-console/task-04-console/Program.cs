﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var fruit = new Dictionary<string, string>();
            var userinput = "";
            var userexit = 0;
            fruit.Add("apple", "");
            fruit.Add("banana", "");
            fruit.Add("orange", "");
            fruit.Add("lemon", "");
            fruit.Add("tomato", "");
            fruit.Add("pumpkin", "");
            var answer = "";
            do
            {
                Console.WriteLine("Enter a fruit to see if it is in stock.");
                userinput = Console.ReadLine();
                if (fruit.TryGetValue(userinput, out answer))
                {
                    Console.WriteLine($"{userinput} is in stock.");
                }
                else
                {
                    Console.WriteLine($"{userinput} is either out of stock or not found.");
                }
                Console.WriteLine("0. Check if another fruit is in stock");
                Console.WriteLine("1. Exit program");
                userexit = int.Parse(Console.ReadLine());
            } while (userexit != 1);
        }
    }
}
