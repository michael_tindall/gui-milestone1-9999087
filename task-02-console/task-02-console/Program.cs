﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var useritems = new double[5] { 0.0, 0.0, 0.0, 0.0, 0.0 };
            var i = 0;
            var j = 0;
            var k = 0;
            var userexit = 0;
            var userinput = 0.0;
            var result = 0.0;
            var resultgst = 0.0;
            Console.WriteLine("Enter the values of items to be calculated with GST. Maximum of 5 items.");
            do
            {
                j = i + 1;
                Console.WriteLine($"You have {i} items.");
                Console.WriteLine($"Enter the price of item {j}");
                userinput = int.Parse(Console.ReadLine());
                useritems[i] = userinput;
                Console.WriteLine("0. Add another item.");
                Console.WriteLine("1. Exit and calculate your items.");
                userexit = int.Parse(Console.ReadLine());
                i++;
            } while ((userexit != 1) && (i < 5));
            result = useritems[0] + useritems[1] + useritems[2] + useritems[3] + useritems[4];
            resultgst = result * 1.15;
            Console.WriteLine($"The total cost of all items is {result}");
            Console.WriteLine($"The total cost of all items plus GST is {resultgst}");
        }
    }
}
